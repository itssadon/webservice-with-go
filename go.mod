module github.com/pluralsight/inventoryservice

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	golang.org/x/net v0.0.0-20210415231046-e915ea6b2b7d
)
